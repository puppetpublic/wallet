define wallet::keytab(
  $kstart_cmd,
  $path,
  $primary = true,
  $mode    = '0600',
  $owner   = 'root',
  $group   = 'root',
  $heimdal = false
) {
  $wallet_opts = "-f '$path' get keytab '$name'"
  exec { "wallet $wallet_opts":
    path    => '/bin:/usr/bin:/usr/local/bin:/usr/kerberos/bin',
    command => "${kstart_cmd} wallet ${wallet_opts}",
    unless  => $heimdal ? {
      true  => "/usr/sbin/ktutil -k '$path' list | grep -i -q '$name'",
      false => "klist -k '$path' | grep -i -q '$name'",
    },
    require => [ Package['kstart'], Package['wallet-client'] ],
  }
  case $primary {
    true, 'true': {
      file { $path:
        mode    => $mode,
        owner   => $owner,
        group   => $group,
        require => Exec["wallet $wallet_opts"],
      }
    }
    false, 'false': { }
    default: {
      crit "Invalid value for primary: $primary (not true or false)"
    }
  }
}
