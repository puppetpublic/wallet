define wallet::other(
  $kstart_cmd,
  $path,
  $type,
  $mode   = '0600',
  $owner  = 'root',
  $group  = 'root',
  $onlyif = 'NONE'
) {
  $wallet_opts = "-f '$path' get '$type' '$name'"
  case $onlyif {
    'NONE': {
      exec { "wallet $wallet_opts":
        path    => '/bin:/usr/bin:/usr/local/bin:/usr/kerberos/bin',
        command => "${kstart_cmd} wallet ${wallet_opts}",
        creates => $path,
        require => [ Package['kstart'], Package['wallet-client'] ],
      }
    }
    default: {
      exec { "wallet $wallet_opts":
        path    => '/bin:/usr/bin:/usr/local/bin:/usr/kerberos/bin',
        command => "${kstart_cmd} wallet ${wallet_opts}",
        onlyif  => $onlyif,
        require => [ Package['kstart'], Package['wallet-client'] ],
      }
    }
  }
  file { $path:
    mode    => $mode,
    owner   => $owner,
    group   => $group,
    require => Exec["wallet $wallet_opts"],
  }
}
