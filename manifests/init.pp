#
# Download objects via the wallet.  It assumes that proper settings have been
# put in /etc/krb5.conf and the ACLs on the objects are set up appropriately.
#
# Examples:
#
#     # Create primary keytab file (default is primary)
#     base::wallet { 'service/adroit-gerbil':
#         path    => '/etc/adroit/gerbil.keytab',
#         owner   => 'leroy',
#         primary => true,
#         ensure  => present,
#     }
#
#     # Add another keytab to the above primary keytab
#     base::wallet { 'service/adroit-gerbil-another':
#         path    => '/etc/adroit/gerbil.keytab',
#         primary => false,
#         require => Base::Wallet['service/adroit-gerbil'],
#         ensure  => present,
#     }
#
#     # Remove the keytab file
#     base::wallet { 'service/funky-chicken':
#         path   => '/etc/funky/chicken.keytab',
#         ensure => absent,
#     }
#
#     # Download a password file.
#     base::wallet { 'unix-foobar-db-baz':
#         path => '/etc/foobar/password',
#         type => 'file',
#     }

# These helper routines are broken out separately to reduce indentation, but
# shouldn't be called separately.  They're purely an implementation detail.

define wallet(
  $ensure         = 'present',
  $auth_keytab    = '/etc/krb5.keytab',
  $auth_principal = 'NA',
  $owner          = 'root',
  $group          = 'root',
  $mode           = '0600',
  $primary        = 'true',
  $type           = 'keytab',
  $onlyif         = 'NONE',
  $heimdal        = false,
  $path
) {
  case $auth_principal {
    'NA': {
      $kstart_cmd = "k5start -Uqf '$auth_keytab' --"
    }
    default: {
      $kstart_cmd = "k5start -qf '$auth_keytab' '$auth_principal' --"
    }
  }

  case $ensure {
    'absent': {
        file { $path: ensure => absent }
    }
    'present': {
      case $type {
        'keytab': {
          wallet::keytab { $name:
            kstart_cmd => $kstart_cmd,
            path       => $path,
            primary    => $primary,
            mode       => $mode,
            owner      => $owner,
            group      => $group,
            heimdal    => $heimdal,
          }
        }
        default: {
          wallet::other { $name:
            kstart_cmd => $kstart_cmd,
            path       => $path,
            type       => $type,
            mode       => $mode,
            owner      => $owner,
            group      => $group,
            onlyif     => $onlyif,
          }
        }
      }
    }
  }
}
